// Make sure we got a filename on the command line.
if (process.argv.length < 3) {
    console.log('Usage: node ' + process.argv[1] + ' FILENAME');
    process.exit(1);
}
// Read the file and print its contents.
var fs = require('fs')
    , readLine = require('readline')
    , filename = process.argv[2];

var readLineInterface = readLine.createInterface({ input: fs.createReadStream(filename, 'utf8') });

readLineInterface.on('line', function (line) {
    let array = line.split(' ');
    let processedCards = processCards(array);
    predictOutcome(array, processedCards);
})

function processCards(array) {
    let cards = [];
    array.forEach(element => {
        card = { value: assignValue(element[0]), suit: element[1] };
        cards.push(card);
    });
    return cards;
}

function assignValue(faceValue) {
    if (faceValue === 'A') return 14;
    if (faceValue === 'T') return 10;
    if (faceValue === 'J') return 11;
    if (faceValue === 'Q') return 12;
    if (faceValue === 'K') {
        return 13;
    } else {
        return Number(faceValue);
    }

}

function predictOutcome(array, cards) {
    let hand = array.splice(0, 5);
    let deck = array;
    if (straightFlush(cards)) {
        return console.log('Hand: ' + hand + ' Deck: ' + deck + ' Best hand: straight-flush');
    } else if (fourOfAKind(cards)) {
        return console.log('Hand: ' + hand + ' Deck: ' + deck + ' Best hand: four-of-a-kind');
    } else if (fullHouse(cards)) {
        return console.log('Hand: ' + hand + ' Deck: ' + deck + ' Best hand: full-house');
    } else if (flush(cards)) {
        return console.log('Hand: ' + hand + ' Deck: ' + deck + ' Best hand: flush');
    } else if (straight(cards)) {
        return console.log('Hand: ' + hand + ' Deck: ' + deck + ' Best hand: straight');
    } else if (threeOfAKind(cards)) {
        return console.log('Hand: ' + hand + ' Deck: ' + deck + ' Best hand: three-of-a-kind');
    } else if (twoPair(cards)) {
        return console.log('Hand: ' + hand + ' Deck: ' + deck + ' Best hand: two-pairs');
    } else if (onePair(cards)) {
        return console.log('Hand: ' + hand + ' Deck: ' + deck + ' Best hand: one-pair');
    } 
}

function straightFlush(cards) {
    let result = false;
    count = {}
    cards.forEach(card => {
        count[card.suit] = (count[card.suit] || 0) + 1;
    });
    if (count.H > 4) {
        let sorted = cards.filter(card =>
            card.suit === 'H'
        ).sort();
        for (let i = 0, j = 0; i < sorted.length; i++) {
            if (sorted[i].value < (sorted[i].value + 1)) {
                result = true;
            } else {
                result = false;
                break;
            }
        }
    } else if (count.C > 4) {
        let sorted = cards.filter(card =>
            card.suit === 'C'
        ).sort();
        for (let i = 0, j = 0; i < sorted.length; i++) {
            if (sorted[i].value < (sorted[i].value + 1)) {
                result = true;
            } else {
                result = false;
                break;
            }
        }
    } else if (count.D > 4) {
        let sorted = cards.filter(card =>
            card.suit === 'D'
        ).sort();
        for (let i = 0, j = 0; i < sorted.length; i++) {
            if (sorted[i].value < (sorted[i].value + 1)) {
                result = true;
            } else {
                result = false;
                break;
            }
        }
    } else if (count.S > 4) {
        let sorted = cards.filter(card =>
            card.suit === 'S'
        ).sort();
        for (let i = 0, j = 0; i < sorted.length; i++) {
            if (sorted[i].value < (sorted[i].value + 1)) {
                result = true;
            } else {
                result = false;
                break;
            }
        }
    }
    return result;
}

function fourOfAKind(cards) {
    count = {};
    cards.forEach(card => {
        count[card.value] = (count[card.value] || 0) + 1;
    });
    return Object.values(count).includes(4);
}

function fullHouse(cards) {
    count = {};
    cards.forEach(card => {
        count[card.value] = (count[card.value] || 0) + 1;
    });
    return Object.values(count).includes(3) && Object.values(count).includes(2);
}

function flush(cards) {
    count = {};
    cards.forEach(card => {
        count[card.suit] = (count[card.suit] || 0) + 1;
    });
    return Object.values(count).includes(5);
}

function straight(cards) {
    result = false;
    count = {};
    values = [];
    finalHand = [];
    cards.forEach(card => {
        count[card.suit] = (count[card.suit] || 0) + 1;
    });
    if (Object.values(count).find(value => value < 5)) {
        sorted = cards.sort((a, b) => a.value - b.value);
        sorted.forEach(card => {
            values.push(card.value);
        });
        uniquesValues = [...new Set(values)];
        console.log(uniquesValues);
        uniquesValues.forEach((value, index) => {
            console.log((uniquesValues[index + 1] - 1));
            if (!(value === (uniquesValues[index + 1] - 1))) {
                finalHand = uniquesValues.splice(index + 1, uniquesValues.length);
            }
        });
        console.log(finalHand);
        finalHand.forEach((value, index) => {
            console.log(value);
            if (value + 1 === finalHand[index + 1]) {
                result = true;
            } else {
                result = false;
            }
        });
    }
    console.log(result)
    return result;
}

function threeOfAKind(cards) {
    count = {};
    cards.forEach(card => {
        count[card.value] = (count[card.value] || 0) + 1;
    });
    return Object.values(count).includes(3)
}

function twoPair(cards) {
    count = {};
    cards.forEach(card => {
        count[card.value] = (count[card.value] || 0) + 1;
    });
    return Object.values(count).includes(2)
}

function onePair(cards) {
    count = {};
    cards.forEach(card => {
        count[card.value] = (count[card.value] || 0) + 1;
    });
    return Object.values(count).includes(1)
}