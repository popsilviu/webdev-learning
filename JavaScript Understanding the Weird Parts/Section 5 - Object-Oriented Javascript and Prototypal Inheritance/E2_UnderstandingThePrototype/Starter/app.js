var person = {
    first: 'default',
    last: 'default',
    getFullName: function() {
        return this.first + ' ' + this.last;
    }
}

var john = {
    first: 'John',
    last: 'Doe'
}

//don't do this ever! perf issues in app

john.__proto__ = person;

console.log(john.getFullName());

console.log(john.first);

jane = {
    first: 'Jane'
}

jane.__proto__ = person;

console.log(jane.getFullName());