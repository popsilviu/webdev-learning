// Whitespace

var
    // first name of the person
    firstname,

    // last name of the person
    lastname,

    // the langauge
    // can be 'en' or 'es'
    language;

var person = {
    firstname: 'John',
    lastname: 'Doe'
}

console.log(person);