var person = {
    first: 'John',
    last: 'Doe',
    getFullName: function() {
        var fullname = this.first + ' ' + this.last
        return fullname;
    }
}

var logName = function(lang1, lang2) {
    console.log('Logged: ' + this.getFullName());
    console.log('Arguments: ' + lang1 + ' ' + lang2);
    console.log('---');
}

var logPersonName = logName.bind(person);

logPersonName('en');

logName.call(person, 'en', 'es');

logName.apply(person, ['en', 'es']);

(function(lang1, lang2) {
    console.log('Logged: ' + this.getFullName());
    console.log('Arguments: ' + lang1 + ' ' + lang2);
    console.log('---');
}).apply(person, ['en', 'es']);

//function borrowing

var person2 = {
    first: 'Jane',
    last: 'Doe'
}

console.log(person.getFullName.apply(person2));

//function currying

function multiply(a, b) {
    return a*b;
}

var multiplyByTwo = multiply.bind(this, 2);
console.log(multiplyByTwo(4));

var multiplyByThree = multiply.bind(this, 3);
console.log(multiplyByTwo(4));

//Function currying - creating a copy of a function but with some preset parameters

