function a() {
    console.log(this);
    this.newvariable = 'hello';
}

var b = function() {
    console.log(this);
}

a();
console.log(newvariable);
b();

var c = {
    name: 'the c obj',
    log: function() {
        var self = this;
        self.name = 'updated obj';
        console.log(self);
        var setname = function(newname) {
            self.name = newname
        }
        setname('update the obj again');
        console.log(self);
    }
}

c.log();