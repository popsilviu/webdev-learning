var tony = {
    firstname: 'Tony',
    lastname: 'Alicea',
    adress: {
        street: 'Somesului 4',
        city: 'Cluj-Napoca',
        state: 'Cluj'
    }
};

function greet(person) {
    console.log('Hi ' + person.firstname);
}

greet(tony);

greet({firstname: 'Mary', lastname: 'Jane'});