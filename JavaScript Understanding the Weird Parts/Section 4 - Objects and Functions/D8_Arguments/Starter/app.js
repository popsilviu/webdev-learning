// arguments - the parameters you pass to a function

function greet(firstname, lastname, language) {

    language = language || 'en';

    if (arguments.length === 0) {
        console.log('Missing parameters');
        return;
    }

    console.log(firstname);
    console.log(lastname);
    console.log(language);
    console.log(arguments);
}

greet();
greet('john');
greet('john', 'doe');
greet('john', 'doe', 'es');