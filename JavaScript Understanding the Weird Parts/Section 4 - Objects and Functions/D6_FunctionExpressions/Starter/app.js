greet();

function greet() {
    console.log('Hi!');
}

var anonGreet = function() {
    console.log('hi');
}

anonGreet();

function log(a) {
    a();
}

log(function(){
    console.log('hello');
});