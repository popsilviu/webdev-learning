// function statemant
function greet(name) {
    console.log('Hello ' + name);
}
greet();

// function expression
var greetFunc = function (name) {
    console.log('Hello ' + name);
};
greetFunc();

// using an Immediately Invoked Function Expression (IIFE)
var greeting = function (name) {
    return 'Hello ' + name;
}('John');

console.log(greeting);

var firstname = 'John';
(function (name) {
    var greeting = 'inside iife: hello';
    console.log(greeting + ' ' + name)
;}(firstname));