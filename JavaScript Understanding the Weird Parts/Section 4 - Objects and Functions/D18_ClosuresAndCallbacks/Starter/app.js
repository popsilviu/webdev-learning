function sayHiLater() {
    var greeting = 'Hi!';
    setTimeout(function(){
        console.log(greeting);
    }, 3000);
}

sayHiLater();

//Callback function - a function you give to antother fucntion, to be run when the other function is finished

function tellMeWhenDone(callback) {
    var a = 1000;
    var b = 2000;

    callback();
}

tellMeWhenDone(function() {
    console.log('I am done!');
});