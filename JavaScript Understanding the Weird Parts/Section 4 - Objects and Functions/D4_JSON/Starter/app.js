var objectLiteral = {
    firstname: 'Mary',
    isAProgrammer: true
};

console.log(objectLiteral);

var jsonValue = JSON.parse('{ "firstname": "Mary", "isAProgrammer": true }');
console.log(jsonValue);