var person = new Object();

person['firstname'] = 'Tony';
person['lastname'] = 'Alicea';

var firstNameProperty = 'firstname';

console.log(person);
console.log(person[firstNameProperty]);

console.log(person.firstname);
console.log(person.lastname);

person.adress = new Object();

person.adress.street = 'Somesului 4';
person.adress.city = 'Cluj-Napoca';
person.adress.state = 'Cluj';

console.log(person.adress.street);
console.log(person.adress.city);
console.log(person['adress']['state']);