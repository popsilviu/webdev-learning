// by value (primitives)

var a = 3;
var b;

b = a;
a = 2;

console.log(a);
console.log(b);

// by reference (all objects (including functions))

var c = { greeting: 'hi'};
var d;

d = c;
c.greeting = 'hello';

console.log(c);
console.log(d);

function changeGreeting(obj) {
    obj.greeting = 'hola';
}

changeGreeting(d);
console.log(c);
console.log(d);

// equals operator sets up new memory space
c = { greeting: 'hey'};

console.log(c);
console.log(d);