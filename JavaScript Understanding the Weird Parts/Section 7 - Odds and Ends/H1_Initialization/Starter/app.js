var people = [
    {
        //the 'john' object
        firstname: 'John',
        lastname: 'Doe',
        address: [
            'somesului',
            'Test'
        ]
    },
    {
        //the 'jane' object
        firstname: 'Jane',
        lastname: 'Doe',
        address: [
            'somesului 4',
            'Test'
        ],
        greet: function() {
            return 'Hello!';
        }
    }
]

console.log(people);