var cars = [
    {
        model: 'Buick',
        price: 'Cheap'
    },
    {
        model: 'Camaro',
        price: 'Expensive'
    }
];

var prices = cars.map(function(car) {
    return car.price;
});

console.log(prices);