var employee = [
    { name: 'Marton', position: 'Dev' },
    { name: 'Lucian', position: 'WebDev' },
    { name: 'Ciprian', position: 'WebDev' }
];

function pluck(array, property) {
    var plucked = array.map(function (element) {
        return element[property];
    });
    return plucked;
}

console.log(pluck(employee, 'position'));