var post = { id: 4, title: 'New Post' };
var comments = [
    {postId: 4, content: 'awesome'},
    {postId: 3, content: 'meh'},
    {postId: 4, content: 'super'},
];

function commentsForPost(post, comments) {
    return comments.filter( function(comment) {
        return comment.postId == post.id;
    });
}

console.log(commentsForPost(post, comments));