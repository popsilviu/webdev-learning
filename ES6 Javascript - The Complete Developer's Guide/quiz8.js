var numbers = [10, 20, 30];

var lessThanFifteen = reject(numbers, function(number) {
    return number > 15;
});

function reject(array, iteratorFunction) {
  return array.filter( function(member) {
      return !iteratorFunction(member);
  })
}

console.log(lessThanFifteen);