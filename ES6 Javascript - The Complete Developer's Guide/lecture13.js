var users = [
    { name: 'Jill' },
    { name: 'Jim' },
    { name: 'Alex' }
];

var foundUser = users.find(function (user) {
    return user.name === 'Alex';
});

console.log(foundUser);