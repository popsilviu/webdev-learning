var images = [
    { height: 10, width: 30 },
    { height: 20, width: 90 },
    { height: 54, width: 32 }
];
var areas = [];

function calculateArea(height, width) {
    let area = height * width;
    return area;
}

images.forEach(function (image) {
    areas.push(calculateArea(image.height, image.width));
});