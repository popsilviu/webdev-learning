var products = [
    {
        name: 'cucumber',
        type: 'vegtable'
    },
    {
        name: 'banana',
        type: 'fruit'
    },
    {
        name: 'celary',
        type: 'vegtable'
    },
    {
        name: 'orange',
        type: 'fruit'
    }
];

var filteredProducts = [];

for (var i = 0; i < products.length; i++) {
    if (products[i].type === 'fruit') {
        filteredProducts.push(products[i]);
    }
};

console.log(filteredProducts);

var filtered = products.filter( function(product) {
    return product.type === 'fruit';
});

console.log(filtered);