const button = document.querySelector('#form__add__comment');
const form = document.querySelector('.timeline__form');
const timelineContent = document.querySelector('.timeline__content');

const addComment = () => {
    event.preventDefault();
    cleanUp();
    let comment = form.querySelector('#form__comment');
    let commentString = comment.value;
    if (commentString === '') {
        comment.style.borderColor = 'red';
    } else {
        createPost(commentString);
        comment.value = '';
    }
}

const createPost = (comment) => {
    var li = document.createElement('li');
    li.classList.add('timeline__content__li');
    timelineContent.appendChild(li);

    var img = document.createElement('img');
    img.classList.add('content__img')
    img.setAttribute('src', 'https://media4.s-nbcnews.com/j/newscms/2016_04/1396246/donald-trump-mug_5fea106e0eb494469a75e60d8f2b18ea.nbcnews-fp-320-320.jpeg');
    li.appendChild(img);

    var textContainer = document.createElement('div');
    textContainer.classList.add('content__container');
    li.appendChild(textContainer);

    var text = document.createElement('p');
    text.classList.add('content__text');
    text.textContent = comment;
    textContainer.appendChild(text);

    var email = document.createElement('span');
    email.classList.add('content__email');
    email.textContent = 'john.doe@nobody.com';
    textContainer.appendChild(email);

    var removeButton = document.createElement('button');
    removeButton.classList.add('content__remove');
    removeButton.textContent = 'x';
    li.appendChild(removeButton);
}

const cleanUp = () => {
    let comment = form.querySelector('#form__comment');
    comment.style.borderColor = null;
}

button.addEventListener('click', addComment);