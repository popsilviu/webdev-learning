const button = document.getElementById('submit');

const validateForm = () => {
    event.preventDefault();
    let formElements = ['body__form__firstname', 'body__form__lastname', 'body__form__message']
    let conformationBanner = document.getElementById('body__confirmation__banner');
    let userInput = [];
    formElements.forEach(element => {
        let input = document.getElementById(element);
        cleanUp(input);
        let inputValue = input.value;
        if (inputValue === '') {
            input.style.borderColor = 'red';
        } else {
            let inputObj = {};
            inputObj[element] = inputValue;
            userInput.push(inputObj);
        }
    });
    if (userInput.length === formElements.length) {
        conformationBanner.style.visibility = 'visible';
        document.getElementById('user__name').textContent = userInput.find(element => {
            return element['body__form__firstname'];
        }).body__form__firstname;
        console.log(userInput);
    }
}

function cleanUp(element) {
    element.style.borderColor = '';
}

button.addEventListener('click', validateForm);