const button = document.getElementById('submit');
const form = document.querySelector('.body__form');
const banner = document.querySelector('#body__confirmation__banner');

const validateForm = () => {
    event.preventDefault();
    let userInput = [];
    const inputFields = form.querySelectorAll('input[type = text]');
    inputFields.forEach(element => {
        cleanUp(element);
        if (checkValidation(element.value)) {
            element.style.borderColor = 'red';
        } else {
            let inputObj = {};
            inputObj[element.id] = element.value;
            userInput.push(inputObj);
        }
    });
    if (userInput.length === inputFields.length) {
        banner.style.visibility = 'visible';
        banner.querySelector('#user__name').textContent = userInput.find(element => {
            return element['body__form__firstname'];
        }).body__form__firstname;
        console.log(userInput);
    }
}

const cleanUp = (element) => {
    element.style.borderColor = null;
}

const checkValidation = (input) => {
    return input === '';
}

button.addEventListener('click', validateForm);