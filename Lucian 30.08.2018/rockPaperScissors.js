function getRandomShape() {
    let shapes = ['rock', 'paper', 'scissors'];
    return shape = shapes[Math.floor(Math.random() * Math.floor(3))];
}

function winner(user, computer) {
    if (user === computer) {
        console.log('It\'s a tie');
    } else if (user === 'scissors' & computer === 'paper') {
        console.log('User WINS!');
    } else if (user === 'paper' & computer === 'rock') {
        console.log('User WINS!');
    } else if (user === 'rock' & computer === 'scissors') {
        console.log('User WINS!');
    } else {
        console.log('Computer WINS!')
    }
}

function play() {
    let user = getRandomShape();
    console.log('User shape is: ' + user);
    let computer = getRandomShape();
    console.log('Computer shape is: ' + computer);
    winner(user, computer);
}

play();